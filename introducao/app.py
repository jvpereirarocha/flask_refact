from flask import Flask, request, Response, redirect, url_for, render_template
from flask_bootstrap import Bootstrap

app = Flask(__name__)
Bootstrap(app)

@app.route("/")
def index():
    return "<a href='/posts'> Posts </a>"

@app.route("/retornar")
def retornar():
    return redirect(url_for("index"))


#Com o objeto response
# @app.route("/response-flask")
# def response_1():
#     headers = {
#         "Content-Type": "text/html",
#     }
#     return Response("Resposta do servidor", 200, headers=headers)


#Sem o objeto response
@app.route("/response_2")
def response_2():
    return render_template("response.html")


@app.route('/posts/<int:id>')
def posts(id):
    titulo = request.args.get('titulo')
    data = {
        'path': request.path,
        'referrer': request.referrer,
        'content_type': request.content_type,
        'method': request.method,
        'titulo': titulo,
        'id': id if id else 0
    }
    return data

# O que vier abaixo não será importado por outros arquivos
if __name__ == '__main__':
    app.run(debug=True) #Setando debug igual a true
